# Identity plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require maymeow/cakephp-identity
```

## Configuration

Load plugin in `Application.php` with

```php
$this->addPlugin('Identity', ['bootstrap' => false, 'routes' => true]);
```

Add to `AppController.php` following code

```php
$this->loadComponent('Auth', [
    //'authorize'=> 'Controller',
    'authenticate' => [
        'Form' => [
            'userModel' => 'Identity.Users', // use this to change default auth model
            'fields' => [
                'username' => 'email',
                'password' => 'password'
            ]
        ]
    ],
    'loginAction' => [
        'controller' => 'Users',
        'action' => 'login',
        'plugin' => 'Identity',
        'prefix' => false
    ],
    // If unauthorized, return them to page they were just on
    'unauthorizedRedirect' => $this->referer()
]);

$this->Auth->deny();
```