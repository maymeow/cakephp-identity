<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $users
 */

 $this->extend('CakeBootstrap./_Common/boxed');
?>


<!-- Hero -->

<div class="row">
    <div class="col-md-12">
        <h1><?= __('Users') ?></h1>
    </div>
</div>

<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <div class="col-md-12">

        <div class="text-right mb-3">
            <?= $this->Html->link(__('New User'), ['action' => 'add'], ['class' => 'btn btn-primary']) ?>
        </div>

        <div class="card">
            <div class="card-body">

                <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm">
                    <thead>
                    <tr>
                                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                                                    <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                                                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                                    <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                    <tr>
                                                                                                                                                                                                                <td><?= $this->Number->format($user->id) ?></td>
                                                                                                                                                                                                                                                                            <td><?= h($user->email) ?></td>
                                                                                                                                                                                                                                                                            <td><?= h($user->password) ?></td>
                                                                                                                                                                                                                                                                            <td><?= h($user->created) ?></td>
                                                                                                                                                                                                                                                                            <td><?= h($user->modified) ?></td>
                                                                                                                                    <td class="actions">
                            <?= $this->Html->link('<i class="far fa-eye"></i>', ['action' => 'view', $user->id], ['escape' => false, 'class' => 'text-dark mr-2']) ?>
                            <?= $this->Html->link('<i class="fas fa-pencil-alt"></i>', ['action' => 'edit', $user->id], ['escape' => false, 'class' => 'text-dark mr-2']) ?>
                            <?= $this->Form->postLink('<i class="far fa-trash-alt"></i>', ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'escape' => false, 'class' => 'text-dark']) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?= $this->Element('CakeBootstrap.pagination') ?>

            </div>
        </div>

    </div>
</div>
<!-- END Page Content -->
