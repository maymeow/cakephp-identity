<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $user
 */
  $this->extend('CakeBootstrap./_Common/boxed');
?>

<!-- Hero -->
<div class="row">
    <div class="col-md-12">
        <h1><?= h($user->id) ?></h1>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="row">
    <div class="col-md-12">

        <div class="text-right mb-3">
            <?= $this->Html->link('<i class="far fa-list-alt"></i>', ['action' => 'index'], ['class' => 'btn btn-light', 'escape' => false]) ?>
            <?= $this->Html->link('<i class="fas fa-pencil-alt"></i>', ['action' => 'edit'], ['class' => 'btn btn-light ml-2', 'escape' => false]) ?>
            <?= $this->Form->postLink(
            '<i class="far fa-trash-alt"></i>',
            ['action' => 'delete', $user->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'btn btn-primary ml-2', 'escape' => false]
            )
            ?>
        </div>

        <div class="card mb-3">
            <div class="card-body">

                <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm">
                                                                                                        <tr>
                                    <th scope="row"><?= __('Email') ?></th>
                                    <td><?= h($user->email) ?></td>
                                </tr>
                                                                                                                <tr>
                                    <th scope="row"><?= __('Password') ?></th>
                                    <td><?= h($user->password) ?></td>
                                </tr>
                                                                                                                                                                    <tr>
                                <th scope="row"><?= __('Id') ?></th>
                                <td><?= $this->Number->format($user->id) ?></td>
                            </tr>
                                                                                                                    <tr>
                                <th scope="row"><?= __('Created') ?></th>
                                <td><?= h($user->created) ?></td>
                            </tr>
                                                    <tr>
                                <th scope="row"><?= __('Modified') ?></th>
                                <td><?= h($user->modified) ?></td>
                            </tr>
                                                                                </table>

            </div>
        </div>

        
                
    </div>
</div>
<!-- END Page Content -->