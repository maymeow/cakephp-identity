<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $user
 */
 $this->extend('CakeBootstrap./_Common/boxed');
?>

<!-- Hero -->
<div class="row">
    <div class="col-md-12">
        <h1><?= __('Users') ?></h1>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="row">
    <div class="col-md-12">

        <div class="text-right mb-3">
            <?= $this->Html->link('<i class="far fa-list-alt"></i>', ['action' => 'index'], ['class' => 'btn btn-light',
            'escape' => false]) ?>
                    </div>

        <div class="card">
            <div class="card-body">

                <?= $this->Form->create($user) ?>


                <?php
                        echo $this->Form->control('email');
                    echo $this->Form->control('password');
                ?>

                <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success mb-3']) ?>
                <?= $this->Form->end() ?>

            </div>
        </div>


    </div>
</div>
<!-- END Page Content -->